package com.dong.common;

public class CommonResult {

    public static final String ERROR_CODE_SUCCESS = "success";
    public static final String ERROR_CODE_FAILED = "fail";
    public static final String ERROR_CODE_RETRY = "retry";

    private String errorCode;
    private String errorMsg;

    public CommonResult() {
        errorCode = ERROR_CODE_SUCCESS;
    }

    public boolean isSuccess() {
        if (getErrorCode().equals(ERROR_CODE_SUCCESS)) {
            return true;
        }
        return false;
    }

    public boolean canRetry() {
        if (getErrorCode().equals(ERROR_CODE_RETRY)) {
            return true;
        }
        return false;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
