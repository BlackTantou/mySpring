package com.dong.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseService {

    public final Logger logger = LoggerFactory.getLogger(getClass());
}
