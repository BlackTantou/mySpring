package com.dong.dao;

import com.dong.entity.User;

import java.util.List;

public interface UserDao {

    public boolean checkPassword(User user);

    public List<User> queryAllUser();

    public User queryUserById(String id);

    public User queryUserByUsername(String username);

    public int insertUser(User user);

    public int updateUser(User user);

    public int deleteUser(String id);
}
