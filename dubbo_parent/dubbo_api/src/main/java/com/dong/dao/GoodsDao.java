package com.dong.dao;

import com.dong.entity.Goods;

import java.util.List;

public interface GoodsDao {

    public List<Goods> queryAllGoods();

    public List<Goods> queryGoodsByType(String goodsType);

    public Goods queryGoodsById(String id);

    public Goods queryGoodsByGoodsNo(String goodsNo);

    public int insertGoods(Goods goods);

    public int updateGoods(Goods goods);

    public int deleteGoods(String goodsId);
}
