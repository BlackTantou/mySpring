package com.dong.dao;

import com.dong.entity.Region;

import java.util.List;

public interface RegionDao {

    public List<Region> queryAllRegion();

    public int insertRegion(Region region);

    public int updateRegion(Region region);

    public int deleteRegion(String areaCode);
}
