package com.dong.test;

import javax.validation.GroupSequence;
import javax.validation.constraints.*;
import javax.validation.groups.Default;
import java.util.Date;
import java.util.List;

public class ValidateTestEntity {
    @NotNull(groups = {Group2.class})
    private String name;
    @Null(groups = {Group2.class})
    private String nullField;
    @AssertFalse(groups = {Group1.class})
    private boolean boolField;
    @Min(value = 2, groups = {Group1.class})
    private String minField;
    @DecimalMin("30")
    private Integer decimalMinField;
    @Size
    private List<String> sizeField;
    @Digits(integer = 2, fraction = 2)
    private Float digitsField;
    @Past
    private Date pastField;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNullField() {
        return nullField;
    }

    public void setNullField(String nullField) {
        this.nullField = nullField;
    }

    public boolean isBoolField() {
        return boolField;
    }

    public void setBoolField(boolean boolField) {
        this.boolField = boolField;
    }

//    public Integer getMinField() {
//        return minField;
//    }
//
//    public void setMinField(Integer minField) {
//        this.minField = minField;
//    }

    public String getMinField() {
        return minField;
    }

    public void setMinField(String minField) {
        this.minField = minField;
    }

    public Integer getDecimalMinField() {
        return decimalMinField;
    }

    public void setDecimalMinField(Integer decimalMinField) {
        this.decimalMinField = decimalMinField;
    }

    public List<String> getSizeField() {
        return sizeField;
    }

    public void setSizeField(List<String> sizeField) {
        this.sizeField = sizeField;
    }

    public Float getDigitsField() {
        return digitsField;
    }

    public void setDigitsField(Float digitsField) {
        this.digitsField = digitsField;
    }

    public Date getPastField() {
        return pastField;
    }

    public void setPastField(Date pastField) {
        this.pastField = pastField;
    }

    @GroupSequence({Default.class, Group2.class, Group1.class})
    public interface Group {
    };

    public interface Group1 {
    }

    public interface Group2 {
    };
}

