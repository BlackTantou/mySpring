package com.dong.test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

public class ValidatorTest {

    public static void main(String[] args) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        ValidateTestEntity validateTestEntity = new ValidateTestEntity();
        validateTestEntity.setNullField("123");
        validateTestEntity.setBoolField(true);
        validateTestEntity.setMinField("0");
        Date d = new Date();
        d.setMonth(7);
        validateTestEntity.setPastField(d);

        System.out.println(validateTestEntity.getMinField());

        Set<ConstraintViolation<ValidateTestEntity>> resultSet = validator.validate(validateTestEntity, ValidateTestEntity.Group1.class,ValidateTestEntity.Group2.class);
        Iterator<ConstraintViolation<ValidateTestEntity>> iterator = resultSet.iterator();
        while (iterator.hasNext()) {
            ConstraintViolation<ValidateTestEntity> t = iterator.next();
            System.out.println(t.getPropertyPath() + t.getMessage());
        }

    }
}
