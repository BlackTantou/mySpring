package com.dong.util;

import org.springframework.util.StringUtils;

import java.util.UUID;

/**
 * 工具类
 */
public class CommonUtil {

    /**
     * 获取32位长度的UUID
     * @return
     */
    public static String getUUID32() {
        return UUID.randomUUID().toString().replace("-", "");
    }

}
