package com.dong.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesUtil {

    @Value("${mySpring.fileUploadPath}")
    private String fileUploadPath;

    public String getFileUploadPath() {
        return fileUploadPath;
    }
}
