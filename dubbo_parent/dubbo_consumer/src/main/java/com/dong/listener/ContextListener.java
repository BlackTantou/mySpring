package com.dong.listener;

import com.dong.service.GoodsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class ContextListener implements ApplicationListener {

    private Logger logger = LoggerFactory.getLogger(ContextListener.class);
    @Autowired
    GoodsService goodsService;

    /**
     * 上下文监听，用于spring加载完毕之后，执行程序初始化操作
     * @param event
     */
    @Override
    public void onApplicationEvent(ApplicationEvent event) {

        if(event instanceof ContextRefreshedEvent){
            if(((ContextRefreshedEvent) event).getApplicationContext().getParent()==null){
                init();
            }
        }
    }

    void init(){
        logger.info("------------------init start------------------");
        goodsService.initGoods(30);
        logger.info("------------------init finish------------------");
    }
}
