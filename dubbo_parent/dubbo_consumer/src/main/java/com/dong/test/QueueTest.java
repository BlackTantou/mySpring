package com.dong.test;

import java.util.concurrent.locks.ReentrantLock;

public class QueueTest {


    Object[] items = new Object[10];
    int index = -1;
    Object empty = new Object();
    Object full = new Object();

    public Object takeItem() {
        synchronized (items) {
            Object o = null;
            try {
                // empty
                while (index == -1) {
                    items.wait();
                }
                o = items[index + 1];
                items[index--] = -1;
                if (index == items.length - 1)
                    items.notifyAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
            }
            return o;
        }

    }

    public boolean putItem(Object o) {
        boolean b = false;
        try {
            //full
            while (index == items.length - 1) {
                items.wait();
            }
            items[++index] = o;
            if (index == -1)
                items.notifyAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
        }
        return b;
    }

    static int i = 0;
    static QueueTest queueTest = new QueueTest();
    static BlockingQueue blockingQueue = new BlockingQueue(10);

    public static void main(String[] args) {

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    int ii = i;
                    queueTest.putItem(i++);
                    try {
//                        blockingQueue.enqueue(i++);
                        System.out.println("put value:" + ii);
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    System.out.println("get value:" + queueTest.takeItem());
                    try {
//                        System.out.println("get value:" + blockingQueue.dequeue());
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        t1.start();
        t2.start();
    }
}
