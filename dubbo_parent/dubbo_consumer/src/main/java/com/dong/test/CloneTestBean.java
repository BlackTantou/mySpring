package com.dong.test;

public class CloneTestBean implements Cloneable {

    CloneTestBean child;
    String name;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public void print() {
//        System.out.println("name=" + name + ";child=" + (child == null ? "null" : child) + ";child.name=" + child.name == null ? "null" : child.name);
        System.out.println("name=" + name + ";child=" + (child == null ? "null" : child)+ ";child.name=" +(child.name == null ? "null" : child.name));
    }
}
