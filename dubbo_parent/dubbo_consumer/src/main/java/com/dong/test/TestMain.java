package com.dong.test;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;


public class TestMain {

    static int index = 0;
    static int[] arr = new int[10];
    static ReentrantLock lock = new ReentrantLock();
    static Condition c1 = lock.newCondition();
    static Condition c2 = lock.newCondition();

    public static void main(String[] args) {
//        CloneTestBean c1 = new CloneTestBean();
//        c1.name = "c1";
//        c1.child = new CloneTestBean();
//        c1.child.name = "c1.child";
//        c1.print();
//        System.out.println(c1);
//        try {
//            CloneTestBean c2 = (CloneTestBean) c1.clone();
//            c2.print();
//            System.out.println(c2);
//        } catch (CloneNotSupportedException e) {
//            e.printStackTrace();
//        }

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    synchronized (c1) {
                        try {
                            lock.lock();
                            while (index == arr.length - 1) {
                                System.out.println("arr is full t1 wait");
//                            c1.await();
                                c1.wait(100);
                            }
//                        c2.signal();
                            System.out.println("t1 put");
                            arr[index] = index + 1;
                            index++;
                            if(index==-1){
                                c1.notifyAll();
                            }
                            System.out.println(Arrays.toString(arr));
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            lock.unlock();
                        }
                    }

                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    synchronized (c1) {
                        try {
                            lock.lock();
                            while (index == -1) {
                                System.out.println("arr is empty t2 wait");
//                            c2.await();
                                c1.wait(100);
                            }
//                        c1.signal();
                            System.out.println("t2 take");
                            int i = arr[index];
                            arr[index] = 0;
                            index--;
                            if(index==arr.length-1){
                                c1.notifyAll();
                            }
                            System.out.println("take value=" + i);
                            System.out.println(Arrays.toString(arr));
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            lock.unlock();
                        }
                    }
                }
            }
        });

//        t1.start();
//        t2.start();

    }

}
