package com.dong.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @RequestMapping(value = "/sayHello")
    public String sayHello(String name) {
        return "hello " + name;
    }

}
