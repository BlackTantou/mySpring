package com.dong.controller;

import com.dong.service.GoodsService;
import com.dong.base.BaseController;
import com.dong.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodsController extends BaseController {

    @Autowired
    GoodsService goodsService;

    @InitBinder("goods")
    public void initUser(WebDataBinder binder) {
        binder.setFieldDefaultPrefix("goods.");
    }


    @RequestMapping("/queryAllGoods")
    public Object queryAllGoods() {
        return goodsService.queryAllGoods();
    }

    @RequestMapping("/buy")
    public Object buy(String id) {
        String buyer = CommonUtil.getUUID32();
        logger.info("buyer:" + buyer);
//        goodsService.buy(buyer, id);
//        return true;
        return goodsService.buy(buyer, id);
    }
}
