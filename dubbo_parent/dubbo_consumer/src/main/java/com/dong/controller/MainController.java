package com.dong.controller;

import com.dong.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MainController extends BaseController {

    @RequestMapping("/")
    public String start() {
        logger.info("index");
        return "index";
    }

    @RequestMapping("/index")
    public String index() {
        logger.info("index");
        return "index";
    }

    @RequestMapping("/unAuthorized")
    public String unAuthorized() {
        logger.info("unAuthorized");
        return "unAuthorized";
    }
}
