package com.dong.controller;

import com.dong.service.RegionService;
import com.dong.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegionController extends BaseController {
    @Autowired
    RegionService regionService;

    @RequestMapping(value = "/queryRegions")
    public Object queryRegions() {
        return regionService.queryAllRegion();
    }
}
