package com.dong.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dong.base.BaseService;
import com.dong.dao.UserDao;
import com.dong.entity.User;
import com.dong.util.MD5Util;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService extends BaseService {

    @Reference
    UserDao userDao;

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public boolean checkPassword(User user) {
        user.setPassword(MD5Util.MD5Encode(user.getPassword()));
        return userDao.checkPassword(user);
    }

    public List<User> queryAllUser() {
        return userDao.queryAllUser();
    }

    public User queryUserById(String id) {
        return userDao.queryUserById(id);
    }

    public User queryUserByUsername(String username) {
        return userDao.queryUserByUsername(username);
    }

    public int insertUser(User user) {
        user.setPassword(MD5Util.MD5Encode(user.getPassword()));
        return userDao.insertUser(user);
    }

    public int updateUser(User user) {
        user.setPassword(MD5Util.MD5Encode(user.getPassword()));
        return userDao.updateUser(user);
    }

    public int deleteUser(String id) {
        return userDao.deleteUser(id);
    }
}
