package com.dong.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dong.base.BaseService;
import com.dong.config.GlobalConfig;
import com.dong.dao.RegionDao;
import com.dong.entity.Region;
import com.dong.redis.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegionService extends BaseService {

    @Autowired
    RedisUtil redisUtil;
    @Reference
    RegionDao regionDao;

    public List<Region> queryAllRegion() {
        if (redisUtil.contains(GlobalConfig.REDIS_KEY_REGION)) {
            return (List<Region>) redisUtil.get(GlobalConfig.REDIS_KEY_REGION);
        }
        List<Region> regions = regionDao.queryAllRegion();
        redisUtil.set(GlobalConfig.REDIS_KEY_REGION, regions);
        return regions;
    }
}
