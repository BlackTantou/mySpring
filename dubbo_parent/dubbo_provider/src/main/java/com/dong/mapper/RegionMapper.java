package com.dong.mapper;

import com.dong.entity.Region;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegionMapper {

    @Select("select area_code as areaCode,area_name as areaName,parent_code as parentCode from region where 1=1")
    List<Region> queryAllRegion();

    @Insert("insert into region(area_code,area_name,parent_code) values(#{areaCode},#{areaName},#{parentCode})")
    int insertRegion(Region region);

    @Delete("delete region where area_code=#{areaCode}")
    int deleteRegion(String areaCode);

    @Update("update region set area_name=#{areaName},parent_code={parentCode} where area_code = #{areaCode}")
    int updateRegion(Region region);
}
