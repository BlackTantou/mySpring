package com.dong.mapper;

import com.dong.entity.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper {

    @Select("select * from user where 1=1")
    List<User> queryAllUser();

    @Select("select * from user where id=#{id}")
    User queryUserById(String id);

    @Select("select * from user where username=#{username}")
    User queryUserByUsername(String username);

    @Insert("insert into user(id,username,password,age,sex,birthday,realname) values(#{id},#{username},#{password},#{age},#{sex},#{birthday},#{realname})")
    int insertUser(User user);

    @Delete("delete from user where id=#{id}")
    int deleteUser(String id);

    @Update("update user set username=#{username},password=#{password},age=#{age},sex=#{sex},birthday=#{birthday},realname=#{realname} where id=#{id}")
    int updateUser(User user);
}
