package com.dong.mapper;

import com.dong.entity.Goods;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoodsMapper {

    @Select("select id as id,goods_name as goodsName,goods_no as goodsNo,goods_type as goodsType,goods_desc as goodsDesc from buss_goods where 1=1")
    List<Goods> queryAllGoods();

    @Select("select id as id,goods_name as goodsName,goods_no as goodsNo,goods_type as goodsType,goods_desc as goodsDesc from buss_goods where goods_type = #{goodsType}")
    List<Goods> queryGoodsByType(String goodsType);

    @Select("select id as id,goods_name as goodsName,goods_no as goodsNo,goods_type as goodsType,goods_desc as goodsDesc from buss_goods where id = #{id}")
    Goods queryGoodsById();

    @Select("select id as id,goods_name as goodsName,goods_no as goodsNo,goods_type as goodsType,goods_desc as goodsDesc from buss_goods where goods_no = #{goodsNo}")
    Goods queryGoodsByGoodsNo(String goodsNo);

    @Insert("insert into buss_goods(goods_name,goods_no,goods_type,goods_desc) values(#{goodsName},#{goodsNo},#{goodsType},#{goodsDesc})")
    int insertGoods(Goods goods);

    @Delete("delete buss_goods where area_code=#{id}")
    int deleteGoods(String areaCode);

    @Update("update buss_goods set goods_name=#{goodsName},goods_no={goodsNo},goods_type={goodsType},goods_desc={goodsDesc} where id = #{id}")
    int updateGoods(Goods goods);
}
