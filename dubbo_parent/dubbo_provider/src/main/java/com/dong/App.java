package com.dong;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.concurrent.CountDownLatch;

/**
 * Hello world!
 */
@EnableCaching
@SpringBootApplication
@EnableDubboConfiguration
@EnableTransactionManagement
@MapperScan("com.dong.mapper")
public class App {
    //使用jar方式打包的启动方式
    private static CountDownLatch countDownLatch = new CountDownLatch(1);

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(App.class, args).registerShutdownHook();
        countDownLatch.await();
    }
}
