package com.dong.daoImpl;

import com.dong.entity.Goods;
import com.dong.mapper.GoodsMapper;
import com.dong.dao.GoodsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service(interfaceClass = GoodsDao.class)
public class GoodsDaoImpl implements GoodsDao {

    @Autowired
    GoodsMapper goodsMapper;

    @Override
    public List<Goods> queryAllGoods() {
        return goodsMapper.queryAllGoods();
    }

    @Override
    public List<Goods> queryGoodsByType(String goodsType) {
        return goodsMapper.queryGoodsByType(goodsType);
    }

    @Override
    public Goods queryGoodsById(String id) {
        return queryGoodsById(id);
    }

    @Override
    public Goods queryGoodsByGoodsNo(String goodsNo) {
        return queryGoodsByGoodsNo(goodsNo);
    }

    @Override
    public int insertGoods(Goods goods) {
        return goodsMapper.insertGoods(goods);
    }

    @Override
    public int updateGoods(Goods goods) {
        return goodsMapper.updateGoods(goods);
    }

    @Override
    public int deleteGoods(String goodsId) {
        return goodsMapper.deleteGoods(goodsId);
    }
}
