package com.dong.daoImpl;

import com.dong.base.BaseDaoImpl;
import com.dong.entity.User;
import com.dong.mapper.UserMapper;
import com.dong.dao.UserDao;
import com.dong.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service(interfaceClass = UserDao.class)
public class UserDaoImpl extends BaseDaoImpl implements UserDao {

    @Autowired
    UserMapper userMapper;

    @Override
    public boolean checkPassword(User user) {
        String passowrd = user.getPassword();
        user = userMapper.queryUserByUsername(user.getUsername());
        if (user == null) {
            logger.error("用户不存在！");
            return false;
        }
        if (passowrd.equals(user.getPassword())) {
            logger.info("密码校验通过！");
            return true;
        }
        logger.error("密码校验不通过！");
        return false;
    }

    public List<User> queryAllUser() {
        return userMapper.queryAllUser();
    }

    public User queryUserById(String id) {
        return userMapper.queryUserById(id);
    }

    public User queryUserByUsername(String username) {
        return userMapper.queryUserByUsername(username);
    }

    @Transactional
    public int insertUser(User user) {
        user.setId(CommonUtil.getUUID32());
        logger.info("insert user.id = " + user.getId());
        return userMapper.insertUser(user);
    }

    public int updateUser(User user) {
        return userMapper.updateUser(user);
    }

    public int deleteUser(String id) {
        return userMapper.deleteUser(id);
    }
}
