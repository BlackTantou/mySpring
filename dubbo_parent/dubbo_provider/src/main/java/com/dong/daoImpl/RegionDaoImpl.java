package com.dong.daoImpl;

import com.dong.entity.Region;
import com.dong.mapper.RegionMapper;
import com.dong.dao.RegionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@com.alibaba.dubbo.config.annotation.Service(interfaceClass = RegionDao.class)
public class RegionDaoImpl implements RegionDao {

    @Autowired
    RegionMapper regionMapper;

    @Override
    public List<Region> queryAllRegion() {
        return regionMapper.queryAllRegion();
    }

    @Override
    public int insertRegion(Region region) {
        return regionMapper.insertRegion(region);
    }

    @Override
    public int updateRegion(Region region) {
        return regionMapper.updateRegion(region);
    }

    @Override
    public int deleteRegion(String areaCode) {
        return regionMapper.deleteRegion(areaCode);
    }
}
